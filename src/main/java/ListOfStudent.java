class ListOfStudent{
    private int studentId;
    private String studentsFirstName;
    private double studentCGPA;
    public ListOfStudent(int id, String name, double cgpa) {
        super();
        this.studentId = id;
        this.studentsFirstName = name;
        this.studentCGPA = cgpa;
    }
    public int getId() {
        return studentId;
    }
    public String getFName() {
        return studentsFirstName;
    }
    public double getCgpa() {
        return studentCGPA;
    }
}