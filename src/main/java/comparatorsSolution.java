import java.util.*;

public class comparatorsSolution {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        int numberOfStudents = Integer.parseInt(scanner.nextLine());

        List<ListOfStudent> studentList = new ArrayList<ListOfStudent>();
        while(numberOfStudents > 0){
            int id = scanner.nextInt();
            String fName = scanner.next();
            double cgpa = scanner.nextDouble();

            ListOfStudent st = new ListOfStudent(id, fName, cgpa);
            studentList.add(st);

            numberOfStudents --;
        }

        Collections.sort(studentList,new Comparator<ListOfStudent>(){
            public int compare(ListOfStudent studentOne, ListOfStudent studentAnother){
                if((studentOne.getCgpa()*100) != (studentAnother.getCgpa()*100)){
                    return (int)((studentAnother.getCgpa()*1000) - (studentOne.getCgpa()*1000));
                }
                else if(!(studentOne.getFName().equals(studentAnother.getFName()))){
                    return studentOne.getFName().compareTo(studentAnother.getFName());
                }
                else{
                    return studentOne.getId()-studentAnother.getId();
                }
            }
        });

        for(ListOfStudent st: studentList){
            System.out.println(st.getFName());
        }
    }
}
